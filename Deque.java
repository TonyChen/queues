/****************************************************************************
 *  Author:         Tony Chen
 *  Written:        2014-06-26
 *  Last updated:   2015-06-26
 *
 *  Compilation:    javac Deque.java
 *  Execution:
 *
 *  A double-ended queue or deque (pronounced "deck") is a generalization of
 *  a stack and a queue that supports inserting and removing items from either
 *  the front or the back of the data structure.
 *
 ***************************************************************************/

import java.util.Iterator;

public class Deque<Item> implements Iterable<Item> {

    private Node first;
    private int count;

    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }

    // construct an empty deque
    public Deque() {
        first = null;
        count = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        // return first == null;
        return count == 0;
    }

    // return the number of items on the deque
    public int size() {
        // if (isEmpty()) return 0;
        //
        // int count = 0;
        // Node curr = first;
        // do {
        //  curr = curr.next;
        //  count++;
        // } while (curr != first);
        //
        return count;
    }

    // insert the item at the front
    public void addFirst(Item item) {
        addLast(item);
        first = first.prev;
    }

    // insert the item at the end
    public void addLast(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }

        Node node = new Node();
        node.item = item;

        if (first == null) {
            first = node;
            first.next = node;
            first.prev = node;
        } else {
            node.next = first;          // next points to first
            node.prev = first.prev;     // prev points to last

            first.prev = node;
            node.prev.next = node;      // last.next points to new node
        }

        count++;
    }

    private Item removeNode(Node node) {
        if (node == first) {
            if (node.next == node) {    // only one node?
                first = null;
            } else {
                first = node.next;
            }
        }

        node.next.prev = node.prev;
        node.prev.next = node.next;

        Item item = node.item;
        node.item = null;               // to avoid loitering
        node.next = null;
        node.prev = null;

        count--;
        return item;
    }

    // delete and return the item at the front
    public Item removeFirst() {
        if (first == null) {
            throw new java.util.NoSuchElementException();
        }
        return removeNode(first);
    }

    // delete and return the item at the end
    public Item removeLast() {
        if (first == null) {
            throw new java.util.NoSuchElementException();
        }
        return removeNode(first.prev);
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = first;

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (current == null) {
                throw new java.util.NoSuchElementException();
            }

            Item item = current.item;

            current = current.next;
            if (current == first) {
                current = null;
            }

            return item;
        }
    }

    // unit testing
    public static void main(String[] args) {
        Deque<String> q = new Deque<String>();
        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            if (item.equals("-")) {
                StdOut.println("-" + q.removeFirst() + " ");
            } else if (item.equals("=")) {
                StdOut.println("=" + q.removeLast() + " ");
            } else if (item.startsWith("+")) {
                q.addFirst(item.substring(1));
            } else {
                q.addLast(item);
            }
        }

        StdOut.println("(" + q.size() + " left on deque)");
        for (String s: q) {
            StdOut.println(s);
        }
    }

}
