/****************************************************************************
 *  Author:         Tony Chen
 *  Written:        2014-06-26
 *  Last updated:   2015-06-26
 *
 *  Compilation:    javac RandomizedQueue.java
 *  Execution:      java RandomizedQueue < sample.txt
 *
 *  A randomized queue is similar to a stack or queue, except that the item
 *  removed is chosen uniformly at random from items in the data structure.
 *
 ***************************************************************************/

import java.util.Iterator;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] a;         // array of items
    private int N;            // number of elements on stack

    // construct an empty randomized queue
    public RandomizedQueue() {
        a = (Item[]) new Object[8];
    }

    // is the queue empty?
    public boolean isEmpty() {
        return N == 0;
    }

    // return the number of items on the queue
    public int size() {
        return N;
    }

    // resize the underlying array holding the elements
    private void resize(int capacity) {
        assert capacity >= N;
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < N; i++) {
            temp[i] = a[i];
        }
        a = temp;
    }

    // add the item
    public void enqueue(Item item) {
        if (item == null) {
            throw new NullPointerException();
        }
        if (N == a.length) resize(2*a.length);  // double size of array if full
        a[N++] = item;                          // add item
    }

    private int random() {
        if (isEmpty()) {
            throw new java.util.NoSuchElementException("Queue empty");
        }
        return StdRandom.uniform(N);
    }

    // delete and return a random item
    public Item dequeue() {
        int i = random();
        Item item = a[i];

        // move a[last] to a[i]
        N--;
        if (i < N) a[i] = a[N];
        a[N] = null;                            // to avoid loitering

        // shrink size of array if necessary, keep at least 8 (2^3) elements
        if (N >= 3 && N == a.length/4) resize(a.length/2);
        return item;
    }

    // return (but do not delete) a random item
    public Item sample() {
        return a[random()];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private int[] index;
        private int idx;

        public RandomizedQueueIterator() {
            if (N > 0) {
                index = new int[N];
                for (int i = 0; i < N; i++) {
                    index[i] = i;
                }
                StdRandom.shuffle(index);
            }
            idx = 0;
        }

        public boolean hasNext() {
            return idx < N;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public Item next() {
            if (idx >= N) {
                throw new java.util.NoSuchElementException();
            }
            return a[index[idx++]];
        }
    }

    // unit testing
    public static void main(String[] args) {
        RandomizedQueue<String> q = new RandomizedQueue<String>();
        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            if (item.equals("-")) {
                StdOut.println("- " + q.dequeue());
            } else if (item.equals("=")) {
                StdOut.println("= " + q.sample());
            } else if (item.equals("*")) {
                StdOut.println("---> iterator (" + q.size()
                            + " left on RandomizedQueue)");
                for (String s: q) {
                    StdOut.println(s);
                }
            } else {
                q.enqueue(item);
            }
        }

        if (q.size() > 0) {
            StdOut.println("---> concurrent list (" + q.size()
                        + " left on RandomizedQueue)");

            Iterator<String> s1 = q.iterator();
            Iterator<String> s2 = q.iterator();
            while (s1.hasNext() && s2.hasNext()) {
                StdOut.println(String.format("%-20s %s", s1.next(), s2.next()));
            }
            if (s1.hasNext() || s2.hasNext()) {
                StdOut.println("*** ERROR: iterator not ending for s1 or s2.");
            } else {
                StdOut.println("---> done.");
            }
        }
    }

}