/****************************************************************************
 *  Author:         Tony Chen
 *  Written:        2014-06-26
 *  Last updated:   2015-06-26
 *
 *  Compilation:    javac Subset.java
 *  Execution:      echo A B C D E F G H I | java Subset 3
 *  Output:         C <NL> G <NL> A <NL>
 *
 *  Takes a command-line integer k; reads in a sequence of N strings from
 *  standard input using StdIn.readString(); and prints out exactly k of
 *  them, uniformly at random. Each item from the sequence can be printed
 *  out at most once.
 *
 ***************************************************************************/

public class Subset {

    public static void main(String[] args) {
        int k = Integer.MAX_VALUE;
        if (args.length > 0) k = Integer.parseInt(args[0]);

        RandomizedQueue<String> q = new RandomizedQueue<String>();
        int i = 0;
        while (!StdIn.isEmpty()) {
            String item = StdIn.readString();
            i++;
            if (i <= k) {
                q.enqueue(item);
            } else {
                if (StdRandom.uniform(i) < k) {
                    q.dequeue();
                    q.enqueue(item);
                }
            }
        }

        while (!q.isEmpty()) {
            StdOut.println(q.dequeue());
        }
    }
}